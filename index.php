<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Calculo Puntaje Elo Fightcade (Prueba)</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	</head>
	<body>		
		<div class="container">
			<h1 class="text-center">Prueba Elo</h1>				
			<div class="alert alert-info" role="alert">			  
				<p>Programita web para calcular el sistema de puntuaci&oacute;n ELO, implementado en fightcade</p>
				<p>En las dos primeras casillas se coloca el puntaje (Score) por ejemplo Player: 10 - Player 2: 0</p>
				<p>En la segunda casilla se coloca el puntaje ELO que comienza en un rango de 400 hacia arriba,. A medida que se coloquen puntos, ira indicando el ranking</p>
				<p>Al presionar calcular, se mostrarán los resultados basados en el player 1 (ejemplo: en caso de qe pierde puntos, el numero será negativo)</p>
				<hr>
				<p class="mb-0">mas info <a href="https://desdelaotravereda.wordpress.com/2021/02/03/anexo-fightcade-2-el-sistema-de-ranking/" target="_blank"><b>Aqu&iacute;</b></a></p>
			</div>
			<div class="row">
				<div class="col-md-8">
					<form id="formulario-elo">
						<div class="form-row">
							<div class="col-md-4 form-group">
								<input type="text" class="form-control" id="player1" name="player1" placeholder="Nick Player 1">
							</div>
							<div class="col-md-4 form-group">
								<input type="number" class="form-control" id="score1" name="score1" placeholder="Score Primer Player" required>
							</div>
							<div class="col-md-4">
								<input type="number" class="form-control" id="score2" name="score2" placeholder="Score Segundo Player" required>								
							</div>
						</div>
						<div class="form-row">
							<div class="col-md-4 form-group">
								<input type="text" class="form-control" id="player1" name="player1" placeholder="Nick Player 1">
							</div>
							<div class="col-md-4 form-group">
								<input type="number" class="form-control" id="elo1" name="elo1" placeholder="Elo Primer Player" required>
								<span class="letra" id="letra1"></span>
							</div>
							<div class="col-md-4 form-group">
								<input type="number" class="form-control" id="elo2" name="elo2" placeholder="Elo Segundo Player" required>
								<span class="letra" id="letra2"></span>
							</div>
						</div>
						<div class="form-row">
							<div class="col-md-8">
								<button type="button" class="btn btn-primary" id="calcular">Calcular y Guardar</button>
								<button type="reset" class="btn btn-warning" id="resetear">Limpiar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div id="resp"></div>
		</div>
		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
		<script src="lib/elo.js" type="text/javascript" charset="utf-8" async defer></script>
	</body>
</html>