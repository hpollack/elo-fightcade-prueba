import os
import json
from openpyxl import *
from elo import *

# Lectura desde un archivo excel con las retaciones
# Se obtienen los valores de esta y se procesan para calcular el  el puntaje relativo Usando el algoritmo ELO de Fightcade
# optimizado para python 
# @author Hermann Pollack
# 

libro = load_workbook("retas2.xlsx")

libro._set_active_sheet_index = 1

retas = libro.active
print(retas)

total_filas = retas.max_row

for i in range(2, total_filas + 1):
	elo1 = 0
	elo2 = 0

	jugador1 = retas.cell(row = i, column = 2).value
	jugador2 = retas.cell(row = i, column = 6).value

	score1 = retas.cell(row = i, column = 3).value
	score2 = retas.cell(row = i, column = 5).value



	a = buscajugador(jugador1)
	b = buscajugador(jugador2)

	print(a, b)
				
	elo1 = a[1]
	elo2 = b[1]	

	print(jugador1, jugador2, score1 , score2, elo1, elo2)
	
	resp = elo(jugador1, jugador2, score1 , score2, elo1, elo2)

	el = json.loads(resp)

	x = actualizaElo(a[0], int(el["res1"]))
	y = actualizaElo(b[0], int(el["res2"]))

	if x == 0 or y == 0:
		exit("No se pudo actualizar tabla")

	respuesta = jugador1 + " (" + str(a[1]) + ") " + str(score1) + " VS " + jugador2 + " ( " + str(b[1]) + ") " + str(score2)	
	# escribirLog(respuesta)
	print(respuesta)

