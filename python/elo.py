import os
import sys
import json
import csv
import mysql.connector

def conectar():
	mydb = mysql.connector.connect(
		host="localhost",
		user="root",
		password="hermann",
		database="fightcade_elo_prueba"
	)

	return mydb

def creaId(tabla, campo):
	tabla = str(tabla)
	campo = str(campo)
	pid = 0
	string = "select max("+ campo +") from " + tabla +";"
	try:
		conn = conectar()
		stmt = conn.cursor()
		stmt.execute(string)
		result = stmt.fetchall()
		for f in result:
			pid = f[0]	
		
	except mysql.connector.Error as e:
		print("Ocurrio un error al conectar a la base de datos", format(e))
		pid = 0
	finally:
		if conn.is_connected():
			stmt.close()
			conn.close()
	return pid		

def buscajugador(player):	
	
	string = "select * from player where nickname = %s"			
	data = []

	try:
		conn = conectar()
		stmt = conn.cursor()
		stmt.execute(string, (player,))
		result = stmt.fetchall()
	
		for f in result:
			data.append(f[0])
			data.append(f[2])			
		
	except mysql.connector.Error as e:
		print("Ocurrio un error al traer los datos", format(error))
		pid = 0		
	finally:
		if conn.is_connected():
			stmt.close()
			conn.close()
	return data

def guardajugador(nickname, juego):
	string	= "insert into player (nickname, pais) values(%s, %s)"
	# pid = creaId("player", "id")
	record = (nickname, "nulo")
	estado = 0
	try:
		conn = conectar()
		stmt = conn.cursor()
		stmt.execute(string, record)
		conn.commit()		
		estado = stmt.lastrowid
	except mysql.connector.Error as e:
		print(format(error))
		estado = 0
	finally:
		if conn.is_connected():
			stmt.close()
			conn.close()
	return estado

def creaElo(idjugador, idjuego):
	string = "insert into elo(player, juego) values (%s, %s);"
	# pid = creaId("elo", "id")
	record = (idjugador, idjuego)
	estado = 0
	try:
		conn = conectar()
		stmt = conn.cursor()
		stmt.execute(string, record)
		conn.commit()
		estado = stmt.lastrowid	
	except mysql.connector.Error as e:
		print(record)		
		print("no se ejecuto la query ", format(e))
		estado = 0
	finally:
		if conn.is_connected():
			stmt.close()
			conn.close()
	return estado

def traeElo(idelo):
	string = "select puntaje_elo from elo where id = %s"
	param = (idelo,)	
	puntaje = 0
	try:
		conn = conectar()
		stmt = conn.cursor()
		stmt.execute(string, param)
		result = stmt.fetchall()
		for f in result:			
			puntaje = f[0]					
	except mysql.connector.Error as e:
		print("Ocurrio un error ", e)
		puntaje = 0		
	finally:
		if conn.is_connected():
			stmt.close()
			conn.close()
	
	return puntaje
def traeIdElo (idjugador, idjuego):
	string = "select id from elo where player = %s and juego = %s"
	param = (idjugador, idjuego)
	pid = 0
	try:
		conn = conectar()
		stmt = conn.cursor()
		stmt.execute(string, param)
		result = stmt.fetchall()
		for f in result:
		 	pid = f[0]
	except mysql.connector.Error as e:
		print("No se pudo traer el id", format(e))
		pid = 0
	finally:
		if conn.is_connected():
			stmt.close()
			conn.close()
	return pid		

def actualizaElo(idelo, puntaje):
	string = "update player set elo = %s where id = %s"
	param  = (puntaje, idelo)
	estado = 0
	try:
		conn = conectar()
		stmt = conn.cursor()
		stmt.execute(string, param)
		conn.commit()
		estado = 1
	except mysql.connector.Error as e:
		print("Ocurrio un error ", format(e))
		estado = 0
	finally:
		if conn.is_connected():
			stmt.close()
			conn.close()

	return estado		

def getK(elo):
	if elo < 700:
		return 34
	if elo < 1000:
		return 32
	if elo < 1300:
		return 30
	if elo < 1600:
		return 28
	if elo < 1900:
		return 26
	return 24	


def elo(jugador1, jugador2, score1, score2, elo1, elo2):
	k1 = getK(elo1)
	k2 = getK(elo2)
	maximo = max(score1, score2)
	score = round( (((score1 - score2) * 0.5 / maximo + 0.5) + sys.float_info.epsilon) * 100) / 100	
	reward = min(2.50, abs((score1 - score2) * 0.25))
	reward = reward * -1 if (score < 0.5) else reward
	chancewin = 1 / (1 + pow(10, (elo2 - elo1) / 400))
	mult = 4 * (score - chancewin)
	points = round(min(k1, k2) * mult * (min(10, maximo) / 10) + reward)
	points = 1 if (points == 0 and score1 > score2) else -1 if (points == 0 and score1 < score2) else points
	res1 = int(elo1) + int(points)
	res2 = int(elo2) - int(points)
	probabilidad = round(chancewin *10000) / 100
	data = {"jugador1": jugador1, "jugador2" : jugador2, "res1" : res1, "res2" : res2, "points" : points, "chance" : probabilidad }
	resp = json.dumps(data)
	texto = str(score1) + ', ' + str(score2) + ', ' + str(elo1) + ', ' + str(elo2) + ', ' + str(k1) + ', ' + str(k2) + ' -> ' + str(points) + ' PUNTOS INTERCAMBIADOS: ' + str(points) + ' - P1_CHANCE_DE_GANAR : ' + str(probabilidad) + '% - RECOMPENSA: ' + str(reward) + ' PUNTOS PARA ' + jugador1 + ' ' + str(elo1) + ' to ' + str(res1) + ' PUNTOS PARA ' + jugador2 + ' ' + str(elo2) + ' to ' + str(res2)
	print(texto)
	escribirLog(texto)
	return resp


def desdeTabla():
	f = open("registro.txt", "a")

	conn = conectar()
	stmt = conn.cursor()

	stmt.execute("select * from retas;")

	result = stmt.fetchall()
	for row in result:
		player1 = row[2]
		player2 = row[4]
		puntos1 = row[3]
		puntos2 = row[5]	
		reta = row[0]

		juego  = 1 # id del juego
		
		ide1   = 0 # id jugador 1
		ide2   = 0 # id jugador 2
		idelo1 = 0 # id puntaje elo jugador 1
		idelo2 = 0 # id puntaje elo jugador 2

		print("Buscando jugador ", player1,"")
		
		pid1 = buscajugador(player1)
		if pid1 == 0:
			print("Jugador", player1, "no existe. Creando registro...")
			ide1 = guardajugador(player1, juego)
			if ide1 != 0:
				print("jugador", player1," creado.")
				pid1 = ide1			
			else:
				print("No se pudo guardar el registro del jugador")
			exit()
		else:
			print("Jugador",pid1, player1," encontrado ")
			print("Buscando si existe elo para ", player1)
			idelo1 = traeIdElo(pid1, juego)	

		print("Buscando jugador ", player2,"")
		
		pid2 = buscajugador(player2)
		if pid2 == 0:
			print("Jugador", player2, "no existe.")
			ide2 = guardajugador(player2, juego)
			if ide2 != 0:
				print("jugador", player2," creado.")
				pid2 = ide2			
			else:
				print("No se pudo guardar el registro del jugador")
			exit()
		else:
			print("Jugador", player2," encontrado")
			print("Buscando si existe elo para ", player2)
			idelo2 = traeIdElo(pid2, juego)


		print("Buscando registros elo de cada jugador...")
		
		elo1 = traeElo(idelo1)
		elo2 = traeElo(idelo2)

		if elo1 == 0:			
			print("No se pudo traer el registro Elo")
			idelo1 = creaElo(pid1, juego)
			elo1 = traeElo(idelo1)				

		if elo2 == 0:
			print("No se pudo traer el registro Elo")
			idelo2 = creaElo(pid2, juego)
			elo2 = traeElo(idelo2)
			
		print("Puntaje jugador ", player1, " : ", elo1, " y Puntaje jugador ", player2, " : ", elo2, "")
		print("resultado de reta: ", player1," ", str(puntos1)," VS ", str(puntos2), " ", player2)
		print("Calculando puntaje Elo...")

		score1 = int(puntos1)
		score2 = int(puntos2)
		elo1 = int(elo1)
		elo2 = int(elo2)

		if score1 != 0 and score2 != 0:
			resp = elo(score1, score2, elo1, elo2)

		elem = json.loads(resp)
			

		nuevo_elo1 = int(elem['res1'])
		nuevo_elo2 = int(elem['res2'])
		chance = elem['chance']
		puntos_int = elem['points']

		f.write(resp)

		update_elo1 = actualizaElo(idelo1, nuevo_elo1)
		update_elo2 = actualizaElo(idelo2, nuevo_elo2)

		if update_elo1 == 1:
			print("Puntaje actual ", player1, " es ", nuevo_elo1)
		else:
			print("No se pudo actualizar")

		if update_elo2 == 1:
			print("Puntaje actual ", player2, " es ", nuevo_elo2)
		else:
			print("No se pudo actualizar")		


	print("Finalizando programa.")
	f.close()

def escribirLog(string):
	f = open("registro.txt", "a")
	f.write(string +"\n")
	f.close()	

# xls = 'retas.xlsx'

# wb = xlrd.open_workbook(xls)

# hoja = wb.sheet_by_index(0)
# print(hoja.nrows)

# score1 = 10
# score2 = 9

# elo1 = 1427
# elo2 = 986

# jugador1 = str(sys.argv[1])
# jugador2 = str(sys.argv[2])

# score1 = int(sys.argv[3])
# score2 = int(sys.argv[4])

# elo1   = int(sys.argv[5])
# elo2   = int(sys.argv[6])




# request = elo(jugador1, jugador2, score1, score2, elo1, elo2)

# el = json.loads(request)

# respuesta = jugador1 + ": " + str(score1) + " VS " + jugador2 + ": " + str(score2) +". " + str(el["res1"]) + " " + str(el["res2"]) +"\n"


# print(respuesta)




