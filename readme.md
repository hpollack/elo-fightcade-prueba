# Simulador Elo Fightcade 2
Programa que utiliza el algoritmo del ranking ELO implementado **Fightcade 2** para calcular los puntajes relativos. 
Se divide de la siguiente forma.

 1. Aplicacion Web sencilla para introducir el puntaje de score y los puntos ELO. Se visualiza el resultado en pantalla.
 2. Aplicación PHP: Scripts de servidor para implementar un ranking con base de datos SQL u otra (en proceso)
 3. Aplicacion Python: Scripts para consola de python 3, que toma un excel y procesa la información para obtener los puntajes y guardarlos en una BD MySQL.
 4. Script MySQL de tablas usadas. En proceso de modificación.

Para mas info los siguientes enlaces:
- [Sistema de Ranking Fightcade en Games and More CL](https://gamesandmore.cl/2021/04/09/anexos-fightcade-2-el-sistema-de-ranking/)
- [Simulador Puntaje Elo](http://www.hpolack.cl/elo/)