/**
 * Retorna el valor K de acuerdo al puntaje ELO recibido
 * @param  {int} elo    Puntaje ELO
 * @return {int}  factor K
 */
function getK (elo) {
    // for rank '?' (<10 games played) the K factor is 20
    if (elo < 700) return 34;   // rank E
    if (elo < 1000) return 32;  // rank D
    if (elo < 1300) return 30;  // rank C
    if (elo < 1600) return 28;  // rank B
    if (elo < 1900) return 26;  // rank A
    return 24;                  // rank S
}

/**
 * Funcion que calcula el Puntaje Elo en Fightcade 2. 
 * @author Staff Fightcade 2
 * @url https://gist.github.com/poliva/77753daaeb15cffd2478341b3257e1b7
 * 
 * @param  {int} score1 Puntos 1st Player
 * @param  {int} score2 Puntos 2nd Player
 * @param  {int} elo1   Elo 1st Player
 * @param  {int} elo2   Elo 2st Player
 * @return {string} cadena con descripcion de los resultados
 */
function eloScore (score1, score2, elo1, elo2) {
    let k1 = getK (elo1)
    let k2 = getK (elo2)
    let max = Math.max(score1, score2)    
    let score = Math.round ( (((score1 - score2) * 0.5 / max + 0.5) + Number.EPSILON) * 100 ) / 100;
    console.log(score)
    let reward =  Math.min(2.50, Math.abs((score1 - score2) * 0.25));
    reward = score < 0.5 ? reward * -1 : reward;
    let chanceToWin = 1 / (1 + Math.pow(10, (elo2 - elo1) / 400 ));
    let multiplier = 4 * (score - chanceToWin);
    let points = Math.round(Math.min(k1, k2) * multiplier * (Math.min(10, max) / 10) + reward)
    points = points === 0 && score1 > score2 ? 1 : points === 0 && score1 < score2 ? -1 : points;
    console.log(score1 + ', ' + score2 + ', ' + elo1 + ', ' + elo2 + ', ' + k1 + ', ' + k2 + ' -> ' + points)
    let res1 = parseInt(elo1)+parseInt(points)
    let res2 = parseInt(elo2)-parseInt(points)    
    console.log('PUNTOS INTERCAMBIADOS: ' + points + ' - CHANCE DE P1: ' + Math.round(chanceToWin * 10000)/100 + '% - REWARD: ' + reward)
    console.log(' PLAYER 1 went from ' + elo1 + ' to ' + res1)
    console.log(' PLAYER 2 went from ' + elo2 + ' to ' + res2)
 
    let resp = score1 + ', ' + score2 + ', ' + elo1 + ', ' + elo2 + ', ' + k1 + ', ' + k2 + ' -> ' + points +
    '<br> PUNTOS INTERCAMBIADOS: ' + points + ' - P1_CHANCE_DE_GANAR : ' + Math.round(chanceToWin * 10000)/100 + '% - RECOMPENSA: ' + reward +
    '<br> PUNTOS PARA PLAYER 1 ' + elo1 + ' to ' + res1 + 
    '<br> PUNTOS PARA PLAYER 2' + elo2 + ' to ' + res2;

    return resp;
}

function designaLetra(elo) {
    let puntaje = parseInt(elo);
    ranking = getK(elo);

    if (ranking == 34) return 'Ranking E';
    if (ranking == 32) return 'Ranking D';
    if (ranking == 30) return 'Ranking C';
    if (ranking == 28) return 'Ranking B';
    if (ranking == 26) return 'Ranking A';
    if (ranking == 24) return 'Ranking S';
    return "";
}

function enviaServer(url, form) {

    let elementos = document.getElementById(form).elements;
    let cadeta = "";
    let campos = "";

    for (var i = 0; i < elementos.length; i++) {
        cadena = campos + elementos[i].name + '=' + encodeURI(elementos[i].value);
        campos = "&";
    }

    let http = new XMLHttpRequest();
    http.open('POST', url, true);
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    http.send(cadena);
}

/*let myArgs = process.argv.slice(2);
if (myArgs[0] === undefined || myArgs[1] === undefined || myArgs[2] === undefined || myArgs[3] === undefined) {
console.log("USAGE: node fc2-elo.js <score1> <score2> <elo1> <elo2>")
console.log("   example: node fc2-elo.js 8 10 1554 1305")
} else {
p1score = parseInt(myArgs[0])
p2score = parseInt(myArgs[1])
p1elo = parseInt(myArgs[2])
p2elo = parseInt(myArgs[3])

eloScore(p1score, p2score, p1elo, p2elo)
}*/

let letra1 = document.getElementById("elo1");
let letra2 = document.getElementById("elo2");

letra1.addEventListener('keyup', function() {
    let span = document.getElementById('letra1');
    let puntaje = document.getElementById('elo1').value;

    if (puntaje != "") {
        let valor = designaLetra(puntaje);
        span.innerText = valor;    
    } else {
        span.innerText = "";
    }    
    
});

letra2.addEventListener('keyup', function() {
    let span = document.getElementById('letra2');
    let puntaje = document.getElementById('elo2').value;

    if (puntaje != "") {
        let valor = designaLetra(puntaje);
        span.innerText = valor;    
    } else {
        span.innerText = "";
    }    
});

let boton = document.getElementById("calcular").addEventListener('click', function() {

    let form = document.getElementById("formulario-elo");

    let s1 = document.getElementById('score1').value;
    let s2 = document.getElementById('score2').value;
    let e1 = document.getElementById('elo1').value;
    let e2 = document.getElementById('elo2').value;

    let div = document.getElementById('resp');

    let p1score = parseInt(s1);
    let p2score = parseInt(s2);
    let p1elo = parseInt(e1)
    let p2elo = parseInt(e2);



    let resp = eloScore(p1score, p2score, p1elo, p2elo);
    // enviaServer('server-elo.php', form);
    div.innerHTML = resp;


});

let limpiar = document.getElementById('resetear').addEventListener('click', function() {
    let resp = document.getElementById('resp');
    let span = document.getElementsByClassName('letra');

    resp.innerHTML = "";
    span[0].innerText = "";
    span[1].innerText = "";
});

