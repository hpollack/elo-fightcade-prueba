<?php
/**
 * Crea conexion PDO
 * @return String cadena de conexion
 */
function conexion() 
{

	$host = "localhost";
	$user = "root";
	$pass = "";
	$db   = "fightcade_elo_prueba";
	
	try 
	{
		
		$cadena = "mysql:host=$host;dbname=$db";			
		$conn =  new PDO($cadena, $user, $pass);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	} 
	catch (PDOException $e) 
	{
		
		exit($e->getMessage());	
	}

	return $conn;
}
/**
 * Genera un id numerico siguiente al ultimo registro de la base de datos
 * @param  String $tabla Nombre de la tabla
 * @param  String $campo Nombre del campo
 * @return Integer      Id siguiente al ultimo registro
 */
function creaId($tabla, $campo)
{

	$conn = conexion();
	$string = "select max(".$campo.") from ".$tabla.";";

	$result = $conn->prepare($string);
	$result->execute();

	if ($f = $result->fetch(PDO::FETCH_NUM))
	{
		
		$max = $f[0];
	}
	
	$conn = null;
	return ($max + 1);
}

/**
 * Trael el facto K de acuerdo al puntaje ELO del jugador
 * Estos valores, se denotan en la letra correspondiente dentro del ranking. Estas son las siguientes
 * - 34 : Ranking E
 * - 32 : Ranking D
 * - 30 : Ranking C
 * - 28 : Ranking B
 * - 26 : Ranking A
 * - 24 : Ranking S  
 * @param  Integer $elo Puntaje actual del jugador
 * @return Integer      Factor K correspondiente
 */
function getk ($elo)
{

	if ($elo < 700) { return 34; }
	if ($elo < 1000) { return 32; }
	if ($elo < 1300) { return 30; }	
	if ($elo < 1600) { return 28; }
	if ($elo < 1900) { return 26; }

	return 24;	
}
/**
 * Funcion que calcula el puntaje elo de cada jugador, una vez finalizada la partida
 * 
 * @param  Integer $score1 Puntos de la partida del jugador 1
 * @param  Integer $score2 Puntos de la partida del jugador 2
 * @param  Integer $elo1   Puntos Elo del jugador 1
 * @param  Integer $elo2   Puntos Elo del jugador 2
 * @return Array         Arreglo con los puntajes resultates para cada jugador y un mensaje que se imprime al final
 */
function eloScore($score1, $score2, $elo1, $elo2)
{

	$k1 = getk($elo1);
	$k2 = getk($elo2);

	$max = max($score1, $score2);
	$score = round(((($score1 - $score2) * 0.5 / $max + 0.5) + PHP_FLOAT_EPSILON) * 100) / 100;
	$reward = min(2.5, abs(($score1 - $score2) * 0.25));
	$reward = ($score < 0.5) ? $reward * -1 : $reward;
	$chancewin = 1 / (1 + pow(10, ($elo2 - $elo1) / 400));
	$mult = 4 * ($score - $chancewin);
	$points = round(min($k1, $k2) * $mult *(min(10, $max) / 10) + $reward);
	$points = ($points === 0 && $score1 > $score2) ? 1 : ($points === 0 && $score1 < $score2) ? -1 : $points;
	$res1 = ($elo1 + $points);
	$res2 = ($elo2 - $points);

	$data = array(
		'res1' => $res1,
		'res2' => $res2	
	);

	/*
		'msg' => ''.$score1 . ', ' . $score2 . ', ' . $elo1 . ', ' . $elo2 . ', ' . $k1 . ', ' . $k2 + ' -> ' . $points . 
		'\n PUNTOS INTERCAMBIADOS: ' . $points . ' - P1_CHANCE_DE_GANAR : ' . round($chancewin * 10000)/100 . '% - RECOMPENSA: ' . $reward . 
		'<br> PUNTOS PARA PLAYER 1 ' . $elo1 . ' to ' . $res1 . 
		'<br> PUNTOS PARA PLAYER 2' . $elo2 . ' to ' . $res2 .''
	 */

	return $data;
}

function traeJugador($nickname)
{

	$conn = conexion();
	$string = "select * from player where nickname = ?";

	$result = $conn->prepare($string);
	$result->execute([$nickname]);
	if ($user = $result->fetch(PDO::FETCH_NUM))
	{
		
		// ID del jugador;
		$id = $user[0];
	}
	else
	{

		$id = 0;
	}

	$conn = null;
	return $id;
}

function guardaJugador($nickname, $juego = 1)
{

	$conn = conexion();
	$resp = false;

	$id = creaId("player", "id");

	$puntaje_default = 1000;	

	$guardaJugador = "insert into player (id, nickname) values (?, ?);";
	$guardaElo = "insert into elo (id, player, juego, puntaje_elo) values (?, ?, ?, ?)";	

	$conn->beginTransaction();
	
	$insert = $conn->prepare($guardaJugador);
	$insert->bindParam(1, $id);
	$insert->bindParam(2, $nickname);	

	if (!$insert->execute())
	{
		
		$conn->rollback();
		$resp = false;
	} 
	else
	{
		
		$idelo = creaId("elo", "id");		
		
		$inselo = $conn->prepare($guardaElo);
		$inselo->bindParam(1,$idelo);
		$inselo->bindParam(2, $id);
		$inselo->bindParam(3, $juego);
		$inselo->bindParam(4, $puntaje_default);

		if (!$inselo->execute())
		{
			
			$conn->rollback();
			$resp = false;
		}
		else
		{

			$conn->commit();
			$resp = true;
		}		
	}

	$conn = null;
	return $resp;
}

function insertaPartida($fecha, $juego = 1, $player1, $puntos1, $player2, $puntos2)
{


	$conn = conexion();
	$string = "insert into retas(id,fecha,juego,player-1,puntos-1,player-2,puntos-2) values(?,?,?,?,?,?,?);";
	$resp = false;

	$id = creaId("retas", "id");

	$conn->beginTransaction();

	$result = $conn->prepare($string);
	$result->bindParam(1, $id);
	$result->bindParam(2, $fecha);
	$result->bindParam(3, $juego);
	$result->bindParam(4, $player1);
	$result->bindParam(5, $puntos1);
	$result->bindParam(6, $player2);
	$result->bindParam(7, $puntos2);

	if (!$result->execute())
	{
		
		$conn->rollback();
		$resp = false;
	}
	else
	{

		$conn->commit();
		$resp = true;
	}

	$conn = null;
	return $resp;
}

function escribeLog($texto)
{

	$ruta = "C:\\tools\\Apache24\\htdocs\\elo-prueba\\registro.log";
	$log = fopen($ruta, "a");

	fwrite($log, "\n".$texto);
	fclose($log);
}