﻿/* SQL Manager for MySQL                              5.7.2.52112 */
/* -------------------------------------------------------------- */
/* Host     : localhost                                           */
/* Port     : 3306                                                */
/* Database : fightcade_elo_prueba                                */


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES 'utf8' */;

SET FOREIGN_KEY_CHECKS=0;

DROP DATABASE IF EXISTS `fightcade_elo_prueba`;

CREATE DATABASE `fightcade_elo_prueba`
    CHARACTER SET 'utf8'
    COLLATE 'utf8_general_ci';

USE `fightcade_elo_prueba`;

/* Structure for the `player` table : */

CREATE TABLE `player` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `nickname` VARCHAR(20) COLLATE utf8_general_ci DEFAULT NULL,
  `pais` VARCHAR(20) COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `nickname` USING BTREE (`nickname`)
) ENGINE=InnoDB
AUTO_INCREMENT=47 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Tabla para jugadores.'
;

/* Structure for the `juego` table : */

CREATE TABLE `juego` (
  `id` INTEGER NOT NULL,
  `nombre` VARCHAR(20) COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY USING BTREE (`id`)
) ENGINE=InnoDB
ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Tabla para los juegos Fightcade rankeados'
;

/* Structure for the `elo` table : */

CREATE TABLE `elo` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `player` INTEGER DEFAULT NULL,
  `juego` INTEGER DEFAULT NULL,
  `puntaje_elo` INTEGER DEFAULT 1000,
  PRIMARY KEY USING BTREE (`id`),
  KEY `elo_fk2` USING BTREE (`juego`),
  KEY `elo_fk1` USING BTREE (`player`),
  CONSTRAINT `elo_fk1` FOREIGN KEY (`player`) REFERENCES `player` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `elo_fk2` FOREIGN KEY (`juego`) REFERENCES `juego` (`id`)
) ENGINE=InnoDB
AUTO_INCREMENT=47 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Tabla con puntaje elo de jugadores.'
;

/* Structure for the `retas` table : */

CREATE TABLE `retas` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `fecha` DATE DEFAULT NULL,
  `juego` INTEGER DEFAULT NULL,
  `player-1` VARCHAR(20) COLLATE utf8_general_ci DEFAULT NULL,
  `puntos-1` INTEGER DEFAULT NULL,
  `player-2` VARCHAR(20) COLLATE utf8_general_ci DEFAULT NULL,
  `puntos-2` INTEGER DEFAULT NULL,
  PRIMARY KEY USING BTREE (`id`)
) ENGINE=InnoDB
AUTO_INCREMENT=156 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Tabla general donde se almacenan las retas. '
;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;