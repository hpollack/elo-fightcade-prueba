<?php
include 'lib/funciones.php';

$conn = conexion();

/*$score1 = $_POST['score1'];
$elo1 = $_POST['elo1'];
$score2 = $_POST['score2'];
$elo2 = $_POST['elo2'];*/

// Por ahora es el unico juego que guardaré
$juego = 1; 

$archivo = fopen("./ftxfatalfury3.csv", "r");

$separador = ";";
$linea = 0;

echo "Comenzando lectura de archivo...";

while (($datos = fgetcsv($archivo, 1000, $separador)) !== false)
{

	
	$num = count($datos);
	$linea++;

	$eloplayer1 = 0;
	$eloplayer2 = 0;

	$fecha = "";
	$player1 = "";
	$puntos1 = "";
	$player2 = "";	
	$puntos2 = "";

	$elo1 = "";
	$elo2 = "";

	echo "Leyendo linea " . $linea ."\n";	
	for ($i=0; $i < $num ; $i++)
	{ 
		
		$fecha = $datos[0];
		$player1 = $datos[1];
		$puntos1 = $datos[2];
		$player2 = $datos[5];	
		$puntos2 = $datos[4];		
	}

	// echo $fecha." - ".$player1.": ".$puntos1." VS ".$player2.": ".$puntos2."\n";

	$existeJugador1 = traeJugador($player1);
	$existeJugador2 = traeJugador($player2);
	
	"Verificando jugador ".$player1."\n";

	if ($existeJugador1 == 0)
	{
	
		echo "No existe aun el jugador ".$player1."\n";
		echo "Creando registro del jugador ".$player1."\n";
		$insertJugador1 = guardaJugador($player1, $juego);
			
		if ($insertJugador1 == false)
		{
			
			exit("No se pudo agregar el jugador. Error en base de datos");
		}

		echo "Registro creado \n";		
	}
	else
	{

		echo "jugador ".$player1." existe.\n";
	}

	"Verificando jugador ".$player2."\n";

	if ($existeJugador2 == 0)
	{
		
		echo "No existe aun el jugador ".$player2."\n";
		echo "Creando registro del jugador ".$player2."\n";
		$insertJugado2 = guardaJugador($player2, $juego);

		if ($insertJugador1 == false)
		{
			
			exit("No se pudo agregar el jugador. Error en base de datos");
		}

		echo "Registro creado \n";	
	}
	else
	{

		echo "jugador ".$player2." existe.\n";
	}

	echo "Trayendo puntajes ELO\n";

	$elo1 = traeElo($player1, $juego);
	$elo2 = traeElo($player2, $juego);	

	if($elo1 == 0)
	{

		exit("No se pudo traer el puntaje ELO del ".$player1.". Error en la base de datos");
	}

	if($elo2 == 0)
	{

		exit("No se pudo traer el puntaje ELO del ".$player2.". Error en la base de datos");
	}

	echo "Resultado partida: ".$fecha." - ".$player1.": ".$puntos1." VS ".$player2.": ".$puntos2."\nCalculando Puntaje ELO\n";

	// Como generó problemas de velocidad de ejecución, se me ocurrió pasar paramétros a python y que devolviera el dato necesario.
	exec("python elo.py $puntos1 $puntos2 $elo1 $elo2", $data, $respuesta);

	if ($respuesta != 0)
	{
		
		exit("Ocurrio un error al ejecutar script");	
	}


	escribeLog("python elo.py $puntos1 $puntos2 $elo1 $elo2 $player1 $player2");

	$json = json_decode($data[1]);	

	/*$res = eloScore((int)$puntos1,(int)$puntos2, (int)$elo1, (int)$elo2);

	for ($i=0; $i < $res ; $i++)
	{ 
		
		$eloplayer1 = $res['res1'];
		$eloplayer2 = $res['res2'];		
	}*/

	$eloplayer1 = $json->res1;
	$eloplayer2 = $json->res2;

	$txt = "Puntaje final Player 1 (".$player1."): ".$eloplayer1.", Puntaje final player 2 (".$player2."): ".$eloplayer2;

	echo $txt."\n";
	// escribeLog($txt);

	echo "\n Actualizando base de datos...\n";	
	$actualizaElo1 = actualizaElo($player1, $eloplayer1);
	if ($actualizaElo1 == false)
	{
		
		exit("No se pudo actualizar ELO $player1. Error base de datos");
	}

	$actualizaElo2 = actualizaElo($player2, $eloplayer2);

	if ($actualizaElo1 == false)
	{
		
		exit("No se pudo actualizar ELO $player2. Error base de datos");
	}

	$txt = $player1." = ".$eloplayer1.", ".$player2.", ".$eloplayer2;
	// escribeLog($txt);

	// echo "Guardando datos de partida...";
	// $insertPartida = insertaPartida($fecha, $juego, $player1, $puntos1, $player2, $puntos2);	
}

fclose($archivo);
echo "Cerrando Archivo. Presione cualquier tecla para continuar";
